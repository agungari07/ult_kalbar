/*
Navicat MySQL Data Transfer

Source Server         : My PC
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : db_ult

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2020-05-21 14:14:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for identitas
-- ----------------------------
DROP TABLE IF EXISTS `identitas`;
CREATE TABLE `identitas` (
  `id_identitas` int(5) NOT NULL AUTO_INCREMENT,
  `nama_website` varchar(100) NOT NULL,
  `alamat_website` varchar(100) NOT NULL,
  `meta_deskripsi` varchar(250) NOT NULL,
  `meta_keyword` varchar(250) NOT NULL,
  `favicon` varchar(50) NOT NULL,
  PRIMARY KEY (`id_identitas`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of identitas
-- ----------------------------
INSERT INTO `identitas` VALUES ('1', 'UNIT LAYANAN TERPADU', 'http://localhost/ult_kalbar', '-', '-', 'favicon.ico');

-- ----------------------------
-- Table structure for tb_pengaduan
-- ----------------------------
DROP TABLE IF EXISTS `tb_pengaduan`;
CREATE TABLE `tb_pengaduan` (
  `id_pengaduan` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `instansi` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `no_telp` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `fasilitas` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `tanggal_pelaksaan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `jumlah_peserta` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `deskripsi` longtext COLLATE latin1_general_ci NOT NULL,
  `file` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_pengaduan`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tb_pengaduan
-- ----------------------------
INSERT INTO `tb_pengaduan` VALUES ('2', 'testing 2', 'sekolah', 'a@gmail.com', '1243-26375475', '- Penginapan<br>- Ruang Pertemuan<br>- Ruang Kelas', '11/11/1314 s.d 21/24/1241', '214', '\r\nSebagai Unit Pelaksana Teknis Kementerian Pendidikan dan Kebudayaan di Provinsi, LPMP Kalimantan Barat memiliki sejumlah fasilitas untuk mendukung pelaksanaan tugas dan fungsinya.\r\n\r\nFasilitas berupa gedung pertemuan, ruang belajar dan penginapan yang ada dapat juga digunakan oleh masyarakat umum dengan dikenakan biaya sesuai tarif yang telah disetujui oleh Kementerian Keuangan Republik Indonesia. Seluruh penerimaan dari biaya peminjaman fasilitas tersebut disetorkan ke Kas Negara untuk menjadi PNBP (Penerimaan Negara Bukan Pajak).\r\n\r\nUntuk memenuhi layanan ini, seluruhmasyarakat dapat mengajukan surat permohonan peminjaman fasilitas ke LPMP Kalimantan Barat untuk ditindaklanjuti sesuai kebutuhan.\r\n', '_SOAL_SOAL_TKD_43.pdf');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin@detik.com', '08238923848', 'admin', 'N', 'ee92j715c3os93mes0353ihq63');
INSERT INTO `users` VALUES ('sinto ', '958f62f9f8b7f348d08943189fda3b15', 'Sinto Gendeng', 'sinto@detik.com', '09945849545', 'user', 'N', 'bc6538b608d50de51baed7e8642d8186');
INSERT INTO `users` VALUES ('joko', '4e5ad0dc4d478726661c8c2b3ea31777', 'Joko Sembung', 'joko@detik.com', '0895485045958', 'user', 'N', '4e5ad0dc4d478726661c8c2b3ea31777');
INSERT INTO `users` VALUES ('wiro', 'fc5db878d663b752550c39f4c919e1fd', 'Wiro Sableng', 'wiro@detik.com', '038039403948', 'user', 'N', 'fc5db878d663b752550c39f4c919e1fd');
INSERT INTO `users` VALUES ('wiros', 'dcdd932ea3418387ef2f06644303389e', 'wiryo', 'wiryo@sableng', '98797', 'user', 'N', '25005d71e4f9a670ebf111888a0916b2');
