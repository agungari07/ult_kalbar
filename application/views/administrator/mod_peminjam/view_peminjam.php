<div class="col-xs-12">  
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Semua Peminjam Fasilitas</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-container" style="width:100%"> 
        <thead>
          <tr>
            <th style='width:20px'>No</th>
            <th>Nama</th>
            <th>Instansi</th>
            <th>No.HP</th>
            <th>Fasilitas</th>
            <th>Tgl Pelaksanaan</th>
            <th>Jumlah Peserta</th>
            <th>File</th>
            <th style='width:50px'>Action</th>
          </tr>
        </thead>
        <tbody>
      <?php 
        $no = 1;
        foreach ($record->result_array() as $row){
        echo "<tr><td>$no</td>
                  <td>$row[nama]</td>
                  <td>$row[instansi]</td>
                  <td>$row[no_telp]</td>
                  <td>$row[fasilitas]</td>
                  <td>$row[tanggal_pelaksaan]</td>
                  <td>$row[jumlah_peserta]</td>
                  <td>$row[file]</td>
                  <td><center>
                    <a href='javascript:;' class='btn btn-warning btn-xs item_donwload' title='Download File' data='$row[id_pengaduan]'><span class='glyphicon glyphicon-hdd'></span></a>
                    <a class='btn btn-success btn-xs' title='Detail Data' href='".base_url()."administrator/edit_peminjam/$row[id_pengaduan]'><span class='glyphicon glyphicon-eye-open'></span></a> <br><br>
                     <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url()."administrator/delete_peminjam/$row[id_pengaduan]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                  </center></td>
              </tr>";
          $no++;
        }
      ?>
      </tbody>
    </table>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
        $('#example1').on('click','.item_donwload',function(){
            var id=$(this).attr('data');
            // window.location.href = ;
            window.open("<?php echo site_url('administrator/donwload_file_peminjam/');?>"+id,"_blank");
        });
    });
 
</script>