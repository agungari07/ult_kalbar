<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Detail Peminjam Fasilitas</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart('',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <tr>
                    	<th width='120px' scope='row'>Nama</th>   
                    	<td>$rows[nama]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Instansi</th>             
                    	<td>$rows[instansi]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Email</th>                    
                    	<td>$rows[email]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>No Telp</th>                  
                    	<td>$rows[no_telp]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Fasilitas</th>                  
                    	<td>$rows[fasilitas]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Tanggal Pelaksanaan</th>                  
                    	<td>$rows[tanggal_pelaksaan]'</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Jumlah Peserta</th>                  
                    	<td>$rows[jumlah_peserta]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Deskripsi</th>                  
                    	<td>$rows[deskripsi]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>File</th>                  
                    	<td><a title='Edit Data' href='".base_url().$this->uri->segment(1)."/donwload_file_peminjam/$rows[id_pengaduan]'>$rows[file]</td>
                    </tr>
                  </tbody>
                  </table>
                </div>
              </div>
                <div class='box-footer'>
                    <a href='".base_url().$this->uri->segment(1)."/peminjam'><button type='button' class='btn btn-default pull-right'>Kembali</button></a>
                    
                  </div>
            </div>";