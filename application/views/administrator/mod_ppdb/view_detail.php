<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Detail Pengaduan PPDB</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart('',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <tr>
                    	<th width='120px' scope='row'>Nama</th>   
                    	<td>$rows[nama]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Kab/Kota</th>             
                    	<td>$rows[kabupaten]</td>
                    </tr>
                    <tr>
                      <th scope='row'>Jenjang</th>             
                      <td>$rows[jenjang]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Email</th>                    
                    	<td>$rows[email]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>No Telp</th>                  
                    	<td>$rows[no_hp]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>Deskripsi</th>                  
                    	<td>$rows[deskripsi]</td>
                    </tr>
                    <tr>
                    	<th scope='row'>File</th>                  
                    	<td><a title='Edit Data' href='".base_url().$this->uri->segment(1)."/donwload_file_ppdb/$rows[id_ppdb]'>$rows[file]</td>
                    </tr>
                  </tbody>
                  </table>
                </div>
              </div>
                <div class='box-footer'>
                    <a href='".base_url().$this->uri->segment(1)."/view'><button type='button' class='btn btn-default pull-left'>Kembali</button></a>
                    <a  onclick='hapus($rows[id_ppdb])'><button type='button' class='btn btn-success pull-right'>Selesai</button></a>
                    
                  </div>
            </div>";
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.3.5/sweetalert2.all.min.js" type="text/javascript"></script>
<script type="text/javascript">
  function hapus(id=null) {
        if(id!=null) {
            swal({
                title : "Apakah anda yakin ingin Data ini Selesai?",
                text  : "Data akan di update.",
                type  : "success",
                showCancelButton   : true,
                confirmButtonColor : "#00a65a",
                confirmButtonText  : "Ya",
                cancelButtonText   : "Batal",
                closeOnConfirm     : false,
                closeOnCancel      : true
            }).then((result) => {
                if (result.value) {
                    window.location.href = "<?php echo base_url('ppdb/update_status/'); ?>" + id;
                }
            });
        }
    }
</script>