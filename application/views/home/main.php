<!-- Main content -->
<div class="row">
  <div class="col-md-12">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class=""></li>
        <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
        <li data-target="#carousel-example-generic" data-slide-to="2" class="active"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item">
          <img src="http://placehold.it/1100x500/39CCCC/ffffff&amp;text=I+Love+Bootstrap" alt="First slide">
          <div class="carousel-caption">
            First Slide
          </div>
        </div>
        <div class="item">
          <img src="http://placehold.it/1100x500/3c8dbc/ffffff&amp;text=I+Love+Bootstrap" alt="Second slide">
          <div class="carousel-caption">
            Second Slide
          </div>
        </div>
        <div class="item active">
          <img src="http://placehold.it/1100x500/f39c12/ffffff&amp;text=I+Love+Bootstrap" alt="Third slide">
          <div class="carousel-caption">
            Third Slide
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="fa fa-angle-left"></span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="fa fa-angle-right"></span>
      </a>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-2 col-xs-6">
    <div class="thumbnail">
      <div class="small-box bg-yellow">
        <div class="icon">
          <i class="fa  fa-home"></i>
        </div>
      </div>
      <div class="caption">
        <h3>PPDB</h3>
        <p><br>Pengaduan PPDB 2020</p>
        <p><a target="_blank" href="<?php echo site_url('pengaduan/ppdb'); ?>" class="btn btn-primary" role="button">Selengkapnya</a></p>
      </div>
    </div>
  </div>
</div>