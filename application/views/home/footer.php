<footer class="main-footer">
    <div class="container">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2020 <a href="https://lpmp-papuabarat.kemdikbud.go.id/">LPMP Papua Barat</a>.</strong> Unit Layanan Terpadu.
    </div>
</footer>