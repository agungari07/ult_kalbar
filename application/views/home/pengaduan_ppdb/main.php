<style type="text/css">
    .green-border {
      border: 1px solid green !important;  
    }

    input:required:focus {
      border: 1px solid red;
      outline: none;
    }

    textarea:required:focus {
      border: 1px solid red;
      outline: none;
    }
</style>
<div class="row">
    <div class="col-md-8">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#prosedur" data-toggle="tab" aria-expanded="false">Prosedur</a></li>
                <li><a href="#formulir" data-toggle="tab" aria-expanded="true">Formulir</a></li>
            </ul>
            <div class="tab-content">
                <?php $this->load->view("home/pengaduan_ppdb/prosedur") ?>
                <?php $this->load->view("home/pengaduan_ppdb/formulir.php") ?>
            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
    </div><!-- /.col -->

    <div class="col-md-4">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $judul_layanan ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque semper dolor neque, ut porta ex maximus in. Phasellus malesuada dui tortor, eget vestibulum mauris suscipit id. Vivamus faucibus ex est, ut accumsan justo volutpat sit amet. Vestibulum sodales libero non ligula vestibulum viverra. Pellentesque mattis lacus leo, non porttitor urna auctor ultrices. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse nec semper dolor. Phasellus sed dui eros. Morbi facilisis neque mi, non sagittis arcu hendrerit a.

                Fusce ut rhoncus mi. Curabitur sit amet ante ut ipsum pharetra tincidunt in sit amet arcu. Integer ipsum lectus, blandit eget mauris nec, luctus facilisis felis. Vivamus feugiat sapien a congue imperdiet. Suspendisse congue orci ligula, non viverra metus aliquet sit amet. Cras fermentum felis ante, ac venenatis turpis ornare a. Donec pulvinar, ex sed placerat vehicula, lectus nulla euismod turpis, eget vehicula risus leo non nisi.
            </div><!-- /.box-body -->
        </div>
    </div><!-- /.col -->
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
    function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 1.1) {
            alert('File Maksimal 1MB');
            $('#kirim').prop('disabled', true);
            // $(file).val(''); //for clearing with Jquery
        } else {
            $('#kirim').prop('disabled', false);
        }
    }

    $(document).ready(function(e) {
        if (<?php echo $flash; ?> == 1) {
            swal("Oops...", "Simpan Data Gagal!", "error");
        }else if(<?php echo $flash; ?> == 2){
            swal("Sukses!", "Simpan Data Berhasil!", "success");

        }
        // $("#save").on('submit', (function(e) {
        //     $('input:required,textarea:required').on('blur', function(){
        //        if($(this).val()!==''){  //assuming the form doesn't have some fields populated by default.
        //             $(this).addClass('green-border');
        //         }else{
        //             $(this).removeClass('green-border');
        //         }
        //     });

        //     e.preventDefault();
        //     $.ajax({
        //         url: "<?php base_url() ?>ppdb/save/",
        //         type: "POST",
        //         data: new FormData(this),
        //         contentType: false,
        //         cache: false,
        //         processData: false,
        //         success: function(data) {
        //             // if (data == 'invalid') {
        //             //     swal("Oops...", "Simpan Data Gagal!", "error");

        //             // } else {
        //             //     swal("Sukses!", "Simpan Data Berhasil!", "success");
        //             //     $("#save")[0].reset();
        //             // }
        //             if ($.isEmptyObject(data.error)) {
        //                 swal("Sukses!", "Simpan Data Berhasil!", "success");
        //                 $("#save")[0].reset();
        //             } else {
        //                 swal("Oops...", "Simpan Data Gagal!", "error");
        //             }
        //         }
        //     });
        // }));
    });
</script>