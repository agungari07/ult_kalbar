  <div class="tab-pane" id="formulir">
      <div class="box box-default">
          <div class="box-header with-border">
              <h3 class="box-title">Formulir <?= $judul_layanan ?></h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form action="<?php echo base_url(); ?>ppdb/save" method="post" enctype="multipart/form-data" id="submit">
              <div class="box-body">
                  <div class="form-group <?= form_error('nama') ? 'has-error' : null ?>">
                      <label>Nama</label>
                      <input type="text" name="nama" id="nama" class="form-control" value="<?= set_value('nama') ?>" placeholder="Enter ...">
                      <?= form_error('nama') ?>
                  </div>
                  <div class="form-group">
                      <label>Kab/Kota</label>
                      <select class="form-control" name="id_kab" id="id_kab">
                          <?php foreach ($kab as $kb) { ?>
                              <option value="<?php echo $kb->id ?>" <?= set_value('id_kab') == $kb->id ? 'selected' : null ?>><?= $kb->name ?></option>
                          <?php } ?>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Jenjang</label>
                      <select class="form-control" name="id_jenjang" id="id_jenjang">
                          <?php foreach ($jenjang as $jj) { ?>
                              <option value="<?php echo $jj->id ?>"><?= $jj->name ?></option>
                          <?php } ?>
                      </select>
                  </div>
                  <div class="form-group <?= form_error('email') ? 'has-error' : null ?>">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" name="email" id="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                      <?= form_error('email') ?>
                  </div>
                  <div class="form-group">
                      <label>No. Handphone</label>
                      <input type="number" name="no_hp" id="no_hp" minlength="12" maxlength="12" class="form-control" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                      <label>Deskripsi</label>
                      <textarea class="form-control" name="deskripsi" id="deskripsi" rows="3" placeholder="Enter ..."></textarea>
                  </div>
                  <div class="form-group">
                      <label for="exampleInputFile">File Pendukung</label>
                      <input type="file" id="myFile" name="filename2" accept="application/pdf, image/jpeg" onchange="ValidateSize(this)">
                      <p class="help-block">Example block-level help text here.</p>
                  </div>
              </div><!-- /.box-body -->

              <div class="box-footer">
                  <button type="submit" name="kirim" id="kirim" class="btn btn-primary">Kirim</button>
              </div>
          </form>
      </div>
  </div><!-- /.tab-pane -->