<!DOCTYPE html>
<html>

<head>
  <?php $this->load->view("home/head.php") ?>
</head>

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">

    <header class="main-header">
      <?php $this->load->view("home/navbar.php") ?>
    </header>
    <div class="content-wrapper">
      <div class="container">
        <section class="content">
          <?php echo $contents; ?>
        </section>
      </div>
    </div>
    <?php $this->load->view("home/footer.php") ?>
  </div>
  <?php $this->load->view("home/script.php") ?>
</body>

</html>