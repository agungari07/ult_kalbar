<section id="sp-section-1">
	<div class="row">
		<div id="sp-title" class="col-lg-12 ">
			<div class="sp-column "></div>
		</div>
	</div>
</section>
<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-2">
					<div class="page-content">
						<section id="section-id-1550883337553" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-5">
										<div id="column-id-1550883337551" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1550883337559" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Layanan Kerjasama</h3>
														<div class="sppb-addon-content"></div>
													</div>
												</div>
												<div id="sppb-addon-1559399235971" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<div class="sppb-addon-content">
															<div style="text-align: left;">Sebagai Unit Pelaksana Teknis Kementerian Pendidikan dan Kebudayaan di Provinsi, <a title="Lembaga Penjaminan Mutu Pendidikan Kalimantan Barat" href="http://lpmpkalbar.id/" target="_blank" rel="noopener noreferrer"><strong>LPMP Kalimantan Barat</strong></a> memiliki rincian tugas yang salah satunya adalah melaksanakan kerja sama di bidang peningkatan mutu pendidikan. Kerjasama dapat dilakukan dengan berbagai stakeholder baik pemerintah daerah, pemerintah pusat lintas kementerian, atau pun masyarakat.</div>
															<div style="text-align: left;">
																<br />Sejak awal berdiri sampai dengan saat ini LPMP berupaya hadir melayani masyarakat khususnya pemerintah daerah untuk membantu penyebarluasan informasi dan program-program peningkatan mutu di daerah.</div>
															<div style="text-align: left;">
																<br />Untuk memenuhi layanan ini, masyarakat khususnya pemerintah daerah dapat mengajukan surat permohonan kerjasama peningkatan mutu ke <a title="Lembaga Penjaminan Mutu Pendidikan Kalimantan Barat" href="http://lpmpkalbar.id/" target="_blank" rel="noopener noreferrer"><strong>LPMP Kalimantan Barat</strong></a> untuk ditindaklanjuti sesuai kebutuhan.</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-7">
										<div id="column-id-1550883337552" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1559398712895" class="clearfix">
													<div class="sppb-addon sppb-addon-tab ">
														<div class="sppb-addon-content sppb-tab tabs-tab">
															<ul class="sppb-nav sppb-nav-tabs">
																<li class="active"><a data-toggle="sppb-tab" class="left " href="#sppb-tab-1559398712895"><span class="sppb-tab-icon"><i class="fa "></i></span> Prosedur Layanan </a>
																</li>
																<li class=""><a data-toggle="sppb-tab" class="left " href="#sppb-tab-1559398712896"> Formulir Layanan </a>
																</li>
															</ul>
															<div class="sppb-tab-content sppb-tab-tabs-content">
																<div id="sppb-tab-1559398712895" class="sppb-tab-pane sppb-fade active in">
																	<div id="sppb-addon-1559400540244" class="clearfix">
																		<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
																			<div class="sppb-addon-content">
																				<div class="sppb-addon-single-image-container">
																					<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/06/01/permohonan-kerjasama.jpg" alt="Image" title="">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div id="sppb-addon-1560084300359" class=" sppb-wow fadeInUp clearfix">
																		<div class="sppb-addon sppb-addon-module ">
																			<div class="sppb-addon-content">
																				<div class="custom">
																					<!-- mibew button -->
																					<p>
																						<a id="mibew-agent-button" href="https://helpdesk.lpmpkalbar.id/chat?locale=id&amp;group=17" target="_blank" rel="noopener noreferrer">
																							<img style="display: block; margin-left: auto; margin-right: auto;" src="https://helpdesk.lpmpkalbar.id/b?i=dua&amp;lang=id&amp;group=17" alt="" border="0" />
																						</a>
																					</p>
																					<!-- / mibew button -->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div id="sppb-tab-1559398712896" class="sppb-tab-pane sppb-fade">
																	<div id="sppb-addon-1559398712938" class="clearfix">
																		<div class="sppb-addon sppb-addon-module ">
																			<div class="sppb-addon-content">
																				<div id="cf_4" class="convertforms cf cf-img-left cf-form-bottom cf-success-hideform  cf-hasLabels  " style="max-width:800px;background-color:rgba(238, 238, 238, 1);border-style:none;border-width:10px;border-color:#f28395;border-radius:0px;padding:0px;font-family:Arial">
																					<form name="cf4" id="cf4" method="post" action="#">
																						<div class="cf-content-wrap cf-col-16 ">
																							<div class="cf-content cf-col-16">
																								<div class="cf-content-img cf-col-16 cf-text-center cf-col-medium-3">
																									<img alt="" class="" style="width:auto;left:0px ;top:0px" src="<?php echo base_url(); ?>/asset/front/images/logo-lpmp-baru-kecil.png" />
																								</div>
																								<div class="cf-content-text cf-col cf-col-medium-13">
																									<h2>Permintaan Kerjasama</h2>
																									<p style="line-height: 22px; text-align: left;"><span style="color: #808080;">Silahkan mengisi formulir dibawah ini untuk permintaan kerjasama<br /></span>
																									</p>
																								</div>
																							</div>
																						</div>
																						<div class="cf-form-wrap cf-col-16 " style="background-color:rgba(195, 195, 195, 1)">
																							<div class="cf-response"></div>
																							<div class="cf-fields">
																								<div class="cf-control-group " data-key="2">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form4_Nama">Nama	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="text" name="cf[Nama]" id="form4_Nama" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="6">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form4_Instansi">Instansi	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="text" name="cf[Instansi]" id="form4_Instansi" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="7">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form4_email">Email	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="email" name="cf[email]" id="form4_email" required class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="8">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form4_telpon">Nomor Telpon	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="tel" name="cf[telpon]" id="form4_telpon" required class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="10">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form4_Penjelasan">Penjelasan Kerjasama Yang diinginkan	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<textarea name="cf[Penjelasan]" id="form4_Penjelasan" required placeholder="" class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px" rows="3"></textarea>
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="12">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form4_Upload">File Upload	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<div class="cfup-tmpl" style="display:none;">
																											<div class="cfup-file">
																												<div class="cfup-status"></div>
																												<div class="cfup-thumb">
																													<!-- <img data-dz-thumbnail /> -->
																												</div>
																												<div class="cfup-details">
																													<div class="cfup-name" data-dz-name></div>
																													<div class="cfup-error">
																														<div data-dz-errormessage></div>
																													</div>
																													<div class="cfup-progress"><span class="dz-upload" data-dz-uploadprogress></span>
																													</div>
																												</div>
																												<div class="cfup-right">	<span class="cfup-size" data-dz-size></span>
																													<a href="#" class="cfup-remove" data-dz-remove>×</a>
																												</div>
																											</div>
																										</div>
																										<div id="form4_Upload" data-name="cf[Upload]" data-key="12" data-maxfilesize="0" data-maxfiles="1" data-acceptedfiles=".pdf" class="cfupload">
																											<div class="dz-message">	<span>Drag and drop files here or</span>
																												<span class="cfupload-browse">Browse</span>
																											</div>
																										</div>
																										<div class="cf-control-input-desc" style="color:#888888">Silahkan Upload Permohonan Resmi dengan Format PDF</div>
																									</div>
																								</div>
																								<div class="cf-control-group cf-one-third" data-key="1">
																									<div class="cf-control-input">
																										<div class="cf-text-center">
																											<button type="submit" class="cf-btn cf-btn-style-gradient cf-btn-shadow-1 cf-one-third" style="border-radius:5px;padding:11px 15px;color:#ffffff;font-size:14px;background-color:#e86161"> <span class="cf-btn-text">Kirim</span>
																												<span class="cf-spinner-container">
            <span class="cf-spinner">
                <span class="bounce1"></span>
																												<span class="bounce2"></span>
																												<span class="bounce3"></span>
																												</span>
																												</span>
																											</button>
																										</div>
																										<style>
																											#cf_4 .cf-btn:after { 
																											            border-radius: 5px
																											        }
																										</style>
																									</div>
																								</div>
																							</div>
																						</div>
																						<input type="hidden" name="cf[form_id]" value="4">
																						<input type="hidden" name="dee74f3eed08a0f135b09407be6eefb4" value="1" />
																						<div class="cf-field-hp">
																							<label for="cf-field-5ec4dd778d4ee" class="cf-label">Phone</label>
																							<input type="text" name="cf[hnpt]" id="cf-field-5ec4dd778d4ee" autocomplete="off" class="cf-input" />
																						</div>
																					</form>
																				</div>
																				<style></style>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>