<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-1">
					<div class="page-content">
						<section id="section-id-1551801943715" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943703" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1551801943718" class="sppb-hidden-xs clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/03/02/zi-lpmp.png" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943704" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1551801943721" class="sppb-hidden-xs clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/03/02/beranijujur.png" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943705" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943706" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943707" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943708" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943709" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943710" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943711" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943712" class="sppb-column">
											<div class="sppb-column-addons"></div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943713" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561859750196" class="sppb-hidden-xs clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<a target="_blank" href="http://www.lpmpkalbar.id/">
																	<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/06/30/logo-lpmp-baru-kecil.png" alt="Image" title="">
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-1">
										<div id="column-id-1551801943714" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561859750199" class="sppb-hidden-xs clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<a target="_blank" href="http://www.kemdikbud.go.id/">
																	<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/06/30/kemdikbud_64x64.png" alt="Image" title="">
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section id="section-id-1550294526865" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-12">
										<div id="column-id-1550294526861" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1551922079007" class=" sppb-wow fadeIn clearfix">
													<div id="sppb-carousel-1551922079007" data-interval="5000" class="sppb-carousel sppb-slide" data-sppb-ride="sppb-carousel">
														<div class="sppb-carousel-inner sppb-text-left">
															<div class="sppb-item sppb-item-15519220790070  sppb-item-has-bg active">
																<img src="<?php echo base_url(); ?>/asset/front/images/2020/04/07/pelayanan-tatap-muka-ult-lpmp-kalbar2.png" alt="">
																<div class="sppb-carousel-item-inner">
																	<div class="sppb-carousel-caption">
																		<div class="sppb-carousel-text"></div>
																	</div>
																</div>
															</div>
															<div class="sppb-item sppb-item-15519220790071  sppb-item-has-bg">
																<img src="<?php echo base_url(); ?>/asset/front/images/2020/02/25/front-2b.png" alt="">
																<div class="sppb-carousel-item-inner">
																	<div class="sppb-carousel-caption">
																		<div class="sppb-carousel-text">
																			<div class="sppb-carousel-content"><strong><span style="font-size: 90pt;">Unit</span><br /><span style="font-size: 32pt;">Layanan Terpadu</span></strong>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="sppb-item sppb-item-15519220790072  sppb-item-has-bg">
																<img src="<?php echo base_url(); ?>/asset/front/images/2020/02/25/front.png" alt="">
																<div class="sppb-carousel-item-inner">
																	<div class="sppb-carousel-caption">
																		<div class="sppb-carousel-text">
																			<div class="sppb-carousel-content"><span style="font-size: 45pt;"><strong>Keseharian</strong></span>
																				<br /><span style="font-size: 28pt;"><strong>Layanan Terpadu<br /></strong></span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section id="section-id-1551829909909" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-2">
										<div id="column-id-1551829909905" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1553111197129" class=" sppb-wow bounceIn clearfix">
													<div class="sppb-addon sppb-addon-feature  ">
														<div class="sppb-addon-content sppb-text-center">
															<div class="sppb-icon"><a href="<?php echo base_url(); ?>layanan-publik/layanan-peminjaman-fasilitas"><span class="sppb-icon-container"><i aria-hidden="true" aria-label="Fasilitas" class="fa fa-institution"></i></span></a>
															</div>
															<div class="sppb-media-content">
																<h2 class="sppb-addon-title sppb-feature-box-title">Fasilitas</h2>
																<div class="sppb-addon-text">
																	<br />Layanan Peminjaman Fasilitas</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-2">
										<div id="column-id-1551829909907" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1551913385540" class=" sppb-wow bounceIn clearfix">
													<div class="sppb-addon sppb-addon-feature  ">
														<div class="sppb-addon-content sppb-text-center">
															<div class="sppb-icon"><a href="<?php echo base_url(); ?>layanan-publik/layanan-permintaan-data-dan-informasi"><span class="sppb-icon-container"><i aria-hidden="true" aria-label="Data & Informasi" class="fa fa-pie-chart"></i></span></a>
															</div>
															<div class="sppb-media-content">
																<h2 class="sppb-addon-title sppb-feature-box-title">Data & Informasi</h2>
																<div class="sppb-addon-text">
																	<br />Layanan Permintaan
																	<br />Data dan Informasi</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-2">
										<div id="column-id-1551829909908" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1551913385545" class=" sppb-wow bounceIn clearfix">
													<div class="sppb-addon sppb-addon-feature  ">
														<div class="sppb-addon-content sppb-text-center">
															<div class="sppb-icon"><a href="<?php echo base_url(); ?>layanan-publik/layanan-kerjasama"><span class="sppb-icon-container"><i aria-hidden="true" aria-label="Kerjasama" class="fa fa-steam"></i></span></a>
															</div>
															<div class="sppb-media-content">
																<h2 class="sppb-addon-title sppb-feature-box-title">Kerjasama</h2>
																<div class="sppb-addon-text">
																	<br />Layanan Pengajuan Kerja Sama</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-2">
										<div id="column-id-1551913385493" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1553111197132" class=" sppb-wow bounceIn clearfix">
													<div class="sppb-addon sppb-addon-feature  ">
														<div class="sppb-addon-content sppb-text-center">
															<div class="sppb-icon"><a href="<?php echo base_url(); ?>layanan-publik/layanan-permintaan-narasumber"><span class="sppb-icon-container"><i aria-hidden="true" aria-label="Narasumber" class="fa fa-street-view"></i></span></a>
															</div>
															<div class="sppb-media-content">
																<h2 class="sppb-addon-title sppb-feature-box-title">Narasumber</h2>
																<div class="sppb-addon-text">
																	<br />Layanan Permintaan Narasumber</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-2">
										<div id="column-id-1551913385548" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1551913385580" class=" sppb-wow bounceIn clearfix">
													<div class="sppb-addon sppb-addon-feature  ">
														<div class="sppb-addon-content sppb-text-center">
															<div class="sppb-icon"><a href="<?php echo base_url(); ?>layanan-publik/layanan-bantuan-supervisi"><span class="sppb-icon-container"><i aria-hidden="true" aria-label="Supervisi" class="fa fa-search"></i></span></a>
															</div>
															<div class="sppb-media-content">
																<h2 class="sppb-addon-title sppb-feature-box-title">Supervisi</h2>
																<div class="sppb-addon-text">
																	<br />Layanan Bantuan Supervisi</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-2">
										<div id="column-id-1555111277215" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561762043267" class=" sppb-wow bounceIn clearfix">
													<div class="sppb-addon sppb-addon-feature  ">
														<div class="sppb-addon-content sppb-text-center">
															<div class="sppb-icon"><span class="sppb-icon-container"><i aria-hidden="true" aria-label="Konsultasi" class="fa fa-users"></i></span>
															</div>
															<div class="sppb-media-content">
																<h2 class="sppb-addon-title sppb-feature-box-title">Konsultasi</h2>
																<div class="sppb-addon-text">
																	<br />Layanan Konsultasi Online</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>