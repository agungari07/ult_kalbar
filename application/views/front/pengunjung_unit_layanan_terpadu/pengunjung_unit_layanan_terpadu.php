<section id="sp-section-1">
	<div class="row">
		<div id="sp-title" class="col-lg-12 ">
			<div class="sp-column "></div>
		</div>
	</div>
</section>
<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-10">
					<div class="page-content">
						<section id="section-id-1562504694499" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-12">
										<div id="column-id-1562504694498" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1562515486352" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Pengunjung Unit Layanan Terpadu LPMP Kalimantan Barat</h3>
														<div class="sppb-addon-content"></div>
													</div>
												</div>
												<div id="sppb-addon-1562504694502" class="clearfix">
													<div class="sppb-addon sppb-addon-module ">
														<div class="sppb-addon-content">
															<div class="custom">
																<iframe width="1200" height="1500" src="https://datastudio.google.com/embed/reporting/13SYcJiuRkQf5Dzk5pscGtoRcYWmIroa-/page/c9iBB" frameborder="0" style="border:0" allowfullscreen></iframe>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>