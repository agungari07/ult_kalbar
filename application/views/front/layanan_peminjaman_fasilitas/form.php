<form name="save" id="save" method="post" action="#">
	<div class="cf-content-wrap cf-col-16 ">
		<div class="cf-content cf-col-16">
			<div class="cf-content-img cf-col-16 cf-text-center cf-col-medium-3">
				<img alt="" class="" style="width:auto;left:0px ;top:0px" src="<?php echo base_url(); ?>/asset/front/images/logo-lpmp-baru-kecil.png" />
			</div>
			<div class="cf-content-text cf-col cf-col-medium-13">
				<h2>Peminjaman Fasilitas</h2>
				<p style="line-height: 22px; text-align: left;"><span style="color: #808080;">Silahkan mengisi formulir dibawah ini untuk peminjaman fasilitas<br /></span>
				</p>
			</div>
		</div>
	</div>
	<div class="cf-form-wrap cf-col-16 " style="background-color:rgba(195, 195, 195, 1)">
		<div class="cf-response"></div>
		<div class="cf-fields">
			<div class="cf-control-group " data-key="2">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_Nama">Nama	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<input type="text" name="Nama" id="form6_Nama" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
				</div>
			</div>
			<div class="cf-control-group " data-key="6">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_Instansi">Instansi	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<input type="text" name="Instansi" id="form6_Instansi" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
				</div>
			</div>
			<div class="cf-control-group cf-one-half" data-key="7">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_email">Email	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<input type="email" name="email" id="form6_email" required class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
				</div>
			</div>
			<div class="cf-control-group cf-one-half" data-key="8">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_telpon">Nomor Telpon	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<input type="tel" name="telpon" id="form6_telpon" required data-inputmask-mask="9999-99999999" class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
				</div>
			</div>
			<div class="cf-control-group " data-key="13">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_Fasilitas">Fasilitas Yang Akan Dipinjam	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<div class="cf-list ">
						<div class="cf-checkbox-group  cf-checkbox-group-required ">
							<input type="checkbox" name="Fasilitas[]" id="form6_Fasilitas_0" value="- Penginapan" required class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
							<label class="cf-label" for="form6_Fasilitas_0" style="font-size: 13px; color: #333333;">Penginapan</label>
						</div>
						<div class="cf-checkbox-group  cf-checkbox-group-required ">
							<input type="checkbox" name="Fasilitas[]" id="form6_Fasilitas_1" value="- Ruang Pertemuan" required class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
							<label class="cf-label" for="form6_Fasilitas_1" style="font-size: 13px; color: #333333;">Ruang Pertemuan</label>
						</div>
						<div class="cf-checkbox-group  cf-checkbox-group-required ">
							<input type="checkbox" name="Fasilitas[]" id="form6_Fasilitas_2" value="- Ruang Kelas" required class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
							<label class="cf-label" for="form6_Fasilitas_2" style="font-size: 13px; color: #333333;">Ruang Kelas</label>
						</div>
					</div>
				</div>
			</div>
			<div class="cf-control-group cf-one-third cf-one-third cf-one-half" data-key="14">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_Mulai">Tanggal Pelaksanaan	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<input type="text" name="Mulai" id="form6_Mulai" required data-inputmask-mask="99/99/9999 s.d 99/99/9999" class="cf-input cf-input-shadow-0 cf-width-auto" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
				</div>
			</div>
			<div class="cf-control-group cf-one-third cf-one-half" data-key="15">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_Peserta">Jumlah Peserta	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<input type="text" name="Peserta" id="form6_Peserta" required data-inputmask-mask="999" class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
				</div>
			</div>
			<div class="cf-control-group " data-key="10">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_Penjelasan">Penjelasan Peminjaman Fasilitas Yang diinginkan	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">
					<textarea name="Penjelasan" id="form6_Penjelasan" required placeholder="" class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px" rows="3"></textarea>
				</div>
			</div>
			<div class="cf-control-group " data-key="12">
				<div class="cf-control-label">
					<label class="cf-label" style="color:#888888;font-size:13px" for="form6_Upload">File Upload	<span class="cf-required-label">*</span>
					</label>
				</div>
				<div class="cf-control-input">

					<div id="form6_Upload" data-name="Upload" data-key="12" data-maxfilesize="0" data-maxfiles="1" data-acceptedfiles=".pdf" class="cfupload">
						<div class="dz-message">	
							<!-- <span>Drag and drop files here or</span> -->
							<!-- <span class="cfupload-browse">Browse</span> -->
							<input class="upload-browse" type="file" id="myFile" name="filename2" accept="application/pdf, image/jpeg" onchange="ValidateSize(this)" required>
						</div>
					</div>
					<div class="cf-control-input-desc" style="color:#888888">Silahkan Upload Permohonan Resmi dengan Format PDF</div>
				</div>
			</div>
			<div class="cf-control-group cf-one-third" data-key="1">
				<div class="cf-control-input">
					<div class="cf-text-center">
						<button id="kirim" type="submit" class="cf-btn-style-gradient cf-btn-shadow-1 cf-one-third" style="border-radius:5px;padding:11px 15px;color:#ffffff;font-size:14px;background-color:#e86161"> <span class="cf-btn-text">Kirim</span>
						</button>
					</div>
					<style>
						#cf_6 .cf-btn:after { 
						            border-radius: 5px
						        }
					</style>
				</div>
			</div>
		</div>
	</div>
	<div class="cf-field-hp">
		<label for="cf-field-5ec4dd6c71a6b" class="cf-label">Email</label>
		<input type="text" name="cf[hnpt]" id="cf-field-5ec4dd6c71a6b" autocomplete="off" class="cf-input" />
	</div>
</form>


<script type="text/javascript">
	function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024; // in MB
        if (FileSize > 1.1) {
            alert('File Maksimal 1MB');
            $('#kirim').prop('disabled', true);
           // $(file).val(''); //for clearing with Jquery
        } else {
            $('#kirim').prop('disabled', false);
        }
    }
</script>