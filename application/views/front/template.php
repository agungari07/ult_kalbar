<!doctype html>
<html prefix="og: http://ogp.me/ns#" lang="id-id" dir="ltr">
<!-- Mirrored from ult.lpmpkalbar.id/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 May 2020 07:33:19 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<!-- /Added by HTTrack -->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="canonical" href="index.html">
	<base />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="Unit Layanan Terpadu LPMP Kalimantan Barat" />
	<meta name="generator" content="Joomla! - Open Source Content Management" />
	<title>Beranda</title>
	<link href="<?php echo base_url(); ?>/asset/front/images/logo-lpmp-baru-icon.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />
	<link href="../cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/components/com_k2/css/k2574e.css?v=2.9.0" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/components/com_sppagebuilder/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/components/com_sppagebuilder/assets/css/animate.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/components/com_sppagebuilder/assets/css/sppagebuilder.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/components/com_sppagebuilder/assets/css/sppagecontainer.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/components/com_sppagebuilder/assets/css/magnific-popup.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/templates/shaper_helixultimate/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/templates/shaper_helixultimate/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/templates/shaper_helixultimate/css/template.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/templates/shaper_helixultimate/css/presets/default.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>/asset/front/media/com_convertforms/css/convertforms1a6c.css?5500e6e34a967114f29f0a6fff97df5b" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.sp-page-builder .page-content #section-id-1551801943715 {
			padding-top: 10px;
			padding-right: 0px;
			padding-bottom: 5px;
			padding-left: 0px;
			margin-top: 0px;
			margin-right: 0px;
			margin-bottom: 0px;
			margin-left: 0px;
		}

		#column-id-1551801943703 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943704 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943705 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943706 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943707 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943708 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943709 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943710 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943711 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943712 {
			box-shadow: 0 0 0 0 #fff;
		}

		#column-id-1551801943713 {
			box-shadow: 0 0 0 0 #fff;
		}

		#sppb-addon-1561859750196 {
			margin-top: 20;
		}

		#column-id-1551801943714 {
			box-shadow: 0 0 0 0 #fff;
		}

		#sppb-addon-1561859750199 {
			margin-top: 20;
		}

		.sp-page-builder .page-content #section-id-1550294526865 {
			padding-top: 0px;
			padding-right: 0px;
			padding-bottom: 5px;
			padding-left: 0px;
			margin-top: 0px;
			margin-right: 0px;
			margin-bottom: 0px;
			margin-left: 0px;
		}

		#column-id-1550294526861 {
			box-shadow: 0 0 0 0 #fff;
		}

		#sppb-addon-1551922079007 {
			margin-top: 20;
		}

		#sppb-addon-1551922079007 #btn-1551922079008.sppb-btn-danger {
			font-weight: 700;
		}

		#sppb-addon-1551922079007 .sppb-item-15519220790071 .sppb-carousel-caption h2 {
			color: #0529bc;
		}

		#sppb-addon-1551922079007 .sppb-item-15519220790071 .sppb-carousel-caption .sppb-carousel-content {
			color: #4c322a;
		}

		#sppb-addon-1551922079007 #btn-1551922079009.sppb-btn-danger {
			font-weight: 700;
		}

		#sppb-addon-1551922079007 .sppb-item-15519220790072 .sppb-carousel-caption .sppb-carousel-content {
			color: #4c322a;
		}

		#sppb-addon-1551922079007 .sppb-carousel-inner>.sppb-item {
			-webkit-transition-duration: 500ms;
			transition-duration: 500ms;
		}

		.sp-page-builder .page-content #section-id-1551829909909 {
			padding-top: 10px;
			padding-right: 0px;
			padding-bottom: 50px;
			padding-left: 0px;
			margin-top: 0px;
			margin-right: 0px;
			margin-bottom: 0px;
			margin-left: 0px;
		}

		#column-id-1551829909905 {
			box-shadow: 0 0 0 0 #fff;
		}

		#sppb-addon-1553111197129 {
			color: #f3f3f3;
			background-color: #4c322a;
			box-shadow: 0px 0px 0px 0px #e6e6e6;
			margin-top: 20;
			margin-bottom: 20;
			padding-bottom: 10;
		}

		#sppb-addon-1553111197129 .sppb-addon-title {
			margin-top: 5px;
			margin-bottom: 5px;
			color: #ffffff;
			font-size: 20px;
			line-height: 20px;
			line-height: 22px;
			font-weight: 600;
		}

		#sppb-addon-1553111197129 .sppb-media-content {
			padding: 10 10 10 10;
		}

		#sppb-addon-1553111197129 .sppb-icon {
			margin-top: 20px;
			margin-bottom: 10px;
		}

		#sppb-addon-1553111197129 .sppb-icon .sppb-icon-container {
			box-shadow: 0px 0px 0px 0px #fff;
			display: inline-block;
			text-align: center;
			padding: 5px 5px 5px 5px;
			color: #ffffff;
		}

		#sppb-addon-1553111197129 .sppb-icon .sppb-icon-container>i {
			font-size: 90px;
			width: 90px;
			height: 90px;
			line-height: 90px;
		}

		@media (min-width:768px) and (max-width:991px) {
			#sppb-addon-1553111197129 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		@media (max-width:767px) {
			#sppb-addon-1553111197129 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		#sppb-addon-1553111197129 {
			transition: .3s;
		}

		#sppb-addon-1553111197129:hover {
			background: #6f4a30;
			box-shadow: 0px 0px 2px 5px #e1e1e1;
		}

		#column-id-1551829909907 {
			box-shadow: 0 0 0 0 #fff;
		}

		#sppb-addon-1551913385540 {
			color: #f3f3f3;
			background-color: #4c322a;
			box-shadow: 0px 0px 0px 0px #e6e6e6;
			margin-top: 20;
			margin-bottom: 20;
			padding-bottom: 10;
		}

		#sppb-addon-1551913385540 .sppb-addon-title {
			margin-top: 5px;
			margin-bottom: 5px;
			color: #ffffff;
			font-size: 20px;
			line-height: 20px;
			line-height: 22px;
			font-weight: 600;
		}

		#sppb-addon-1551913385540 .sppb-media-content {
			padding: 10 10 10 10;
		}

		#sppb-addon-1551913385540 .sppb-icon {
			margin-top: 20px;
			margin-bottom: 10px;
		}

		#sppb-addon-1551913385540 .sppb-icon .sppb-icon-container {
			box-shadow: 0px 0px 0px 0px #fff;
			display: inline-block;
			text-align: center;
			padding: 5px 5px 5px 5px;
			color: #ffffff;
		}

		#sppb-addon-1551913385540 .sppb-icon .sppb-icon-container>i {
			font-size: 90px;
			width: 90px;
			height: 90px;
			line-height: 90px;
		}

		@media (min-width:768px) and (max-width:991px) {
			#sppb-addon-1551913385540 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		@media (max-width:767px) {
			#sppb-addon-1551913385540 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		#sppb-addon-1551913385540 {
			transition: .3s;
		}

		#sppb-addon-1551913385540:hover {
			background: #6f4a30;
			box-shadow: 0px 0px 2px 5px #e1e1e1;
		}

		#column-id-1551829909908 {
			box-shadow: 0 0 0 0 #fff;
		}

		#sppb-addon-1551913385545 {
			color: #f3f3f3;
			background-color: #4c322a;
			box-shadow: 0px 0px 0px 0px #e6e6e6;
			margin-top: 20;
			margin-bottom: 20;
			padding-bottom: 10;
		}

		#sppb-addon-1551913385545 .sppb-addon-title {
			margin-top: 5px;
			margin-bottom: 5px;
			color: #ffffff;
			font-size: 20px;
			line-height: 20px;
			line-height: 22px;
			font-weight: 600;
		}

		#sppb-addon-1551913385545 .sppb-media-content {
			padding: 10 10 10 10;
		}

		#sppb-addon-1551913385545 .sppb-icon {
			margin-top: 20px;
			margin-bottom: 10px;
		}

		#sppb-addon-1551913385545 .sppb-icon .sppb-icon-container {
			box-shadow: 0px 0px 0px 0px #fff;
			display: inline-block;
			text-align: center;
			padding: 5px 5px 5px 5px;
			color: #ffffff;
		}

		#sppb-addon-1551913385545 .sppb-icon .sppb-icon-container>i {
			font-size: 90px;
			width: 90px;
			height: 90px;
			line-height: 90px;
		}

		@media (min-width:768px) and (max-width:991px) {
			#sppb-addon-1551913385545 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		@media (max-width:767px) {
			#sppb-addon-1551913385545 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		#sppb-addon-1551913385545 {
			transition: .3s;
		}

		#sppb-addon-1551913385545:hover {
			background: #6f4a30;
			box-shadow: 0px 0px 2px 5px #e1e1e1;
		}

		#sppb-addon-1553111197132 {
			color: #f3f3f3;
			background-color: #4c322a;
			box-shadow: 0px 0px 0px 0px #e6e6e6;
			margin-top: 20;
			margin-bottom: 20;
			padding-bottom: 10;
		}

		#sppb-addon-1553111197132 .sppb-addon-title {
			margin-top: 5px;
			margin-bottom: 5px;
			color: #ffffff;
			font-size: 20px;
			line-height: 20px;
			line-height: 22px;
			font-weight: 600;
		}

		#sppb-addon-1553111197132 .sppb-media-content {
			padding: 10 10 10 10;
		}

		#sppb-addon-1553111197132 .sppb-icon {
			margin-top: 20px;
			margin-bottom: 10px;
		}

		#sppb-addon-1553111197132 .sppb-icon .sppb-icon-container {
			box-shadow: 0px 0px 0px 0px #fff;
			display: inline-block;
			text-align: center;
			padding: 5px 5px 5px 5px;
			color: #ffffff;
		}

		#sppb-addon-1553111197132 .sppb-icon .sppb-icon-container>i {
			font-size: 90px;
			width: 90px;
			height: 90px;
			line-height: 90px;
		}

		@media (min-width:768px) and (max-width:991px) {
			#sppb-addon-1553111197132 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		@media (max-width:767px) {
			#sppb-addon-1553111197132 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		#sppb-addon-1553111197132 {
			transition: .3s;
		}

		#sppb-addon-1553111197132:hover {
			background: #6f4a30;
			box-shadow: 0px 0px 2px 5px #e1e1e1;
		}

		#sppb-addon-1551913385580 {
			color: #f3f3f3;
			background-color: #4c322a;
			box-shadow: 0px 0px 0px 0px #e6e6e6;
			margin-top: 20;
			margin-bottom: 20;
			padding-bottom: 10;
		}

		#sppb-addon-1551913385580 .sppb-addon-title {
			margin-top: 5px;
			margin-bottom: 5px;
			color: #ffffff;
			font-size: 20px;
			line-height: 20px;
			line-height: 22px;
			font-weight: 600;
		}

		#sppb-addon-1551913385580 .sppb-media-content {
			padding: 10 10 10 10;
		}

		#sppb-addon-1551913385580 .sppb-icon {
			margin-top: 20px;
			margin-bottom: 10px;
		}

		#sppb-addon-1551913385580 .sppb-icon .sppb-icon-container {
			box-shadow: 0px 0px 0px 0px #fff;
			display: inline-block;
			text-align: center;
			padding: 5px 5px 5px 5px;
			color: #ffffff;
		}

		#sppb-addon-1551913385580 .sppb-icon .sppb-icon-container>i {
			font-size: 90px;
			width: 90px;
			height: 90px;
			line-height: 90px;
		}

		@media (min-width:768px) and (max-width:991px) {
			#sppb-addon-1551913385580 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		@media (max-width:767px) {
			#sppb-addon-1551913385580 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		#sppb-addon-1551913385580 {
			transition: .3s;
		}

		#sppb-addon-1551913385580:hover {
			background: #6f4a30;
			box-shadow: 0px 0px 2px 5px #e1e1e1;
		}

		#sppb-addon-1561762043267 {
			color: #f3f3f3;
			background-color: #4c322a;
			box-shadow: 0px 0px 0px 0px #e6e6e6;
			margin-top: 20;
			margin-bottom: 20;
			padding-bottom: 10;
		}

		#sppb-addon-1561762043267 .sppb-addon-title {
			margin-top: 5px;
			margin-bottom: 5px;
			color: #ffffff;
			font-size: 20px;
			line-height: 20px;
			line-height: 22px;
			font-weight: 600;
		}

		#sppb-addon-1561762043267 .sppb-media-content {
			padding: 10 10 10 10;
		}

		#sppb-addon-1561762043267 .sppb-icon {
			margin-top: 20px;
			margin-bottom: 10px;
		}

		#sppb-addon-1561762043267 .sppb-icon .sppb-icon-container {
			box-shadow: 0px 0px 0px 0px #fff;
			display: inline-block;
			text-align: center;
			padding: 5px 5px 5px 5px;
			color: #ffffff;
		}

		#sppb-addon-1561762043267 .sppb-icon .sppb-icon-container>i {
			font-size: 90px;
			width: 90px;
			height: 90px;
			line-height: 90px;
		}

		@media (min-width:768px) and (max-width:991px) {
			#sppb-addon-1561762043267 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		@media (max-width:767px) {
			#sppb-addon-1561762043267 .sppb-media .sppb-media-body {
				width: auto;
			}
		}

		#sppb-addon-1561762043267 {
			transition: .3s;
		}

		#sppb-addon-1561762043267:hover {
			background: #6f4a30;
			box-shadow: 0px 0px 2px 5px #e1e1e1;
		}

		h1 {
			font-family: 'Arial', sans-serif;
		}

		.logo-image {
			height: 59px;
		}

		.logo-image-phone {
			height: 59px;
		}

		.logo-image {
			height: 59px;
		}

		.logo-image-phone {
			height: 59px;
		}
	</style>
	<script src="<?php echo base_url(); ?>asset/front/media/jui/js/jquery.min1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/jui/js/jquery-noconflict1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/jui/js/jquery-migrate.min1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/k2/assets/js/k2.frontend4cb4.js?v=2.9.0&amp;sitepath=/" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/system/js/core1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/components/com_sppagebuilder/assets/js/sppagebuilder.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/components/com_sppagebuilder/assets/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]><script src="/media/system/js/polyfill.event.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script><![endif]-->
	<script src="<?php echo base_url(); ?>asset/front/media/system/js/keepalive1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/com_convertforms/js/convertforms1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/com_convertforms/js/inputmask1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/com_convertforms/js/checkbox1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/media/com_convertforms/js/dropzone.min1a6c.js?5500e6e34a967114f29f0a6fff97df5b" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/templates/shaper_helixultimate/js/popper.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/templates/shaper_helixultimate/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>asset/front/templates/shaper_helixultimate/js/main.js" type="text/javascript"></script>
	<script type="text/javascript">
		var ConvertFormsConfig = {
			"baseurl": "http://localhost/ult_kalbar/",
			"token": "dee74f3eed08a0f135b09407be6eefb4",
			"debug": false,
		};

		template = "shaper_helixultimate";
	</script>
	<meta content="Beranda" property="og:title" />
	<meta content="website" property="og:type" />
	<meta content="http://ult.lpmpkalbar.id/" property="og:url" />
</head>

<body class="site helix-ultimate com-sppagebuilder view-page layout-default task-none itemid-101 id-id ltr sticky-header layout-fluid offcanvas-init offcanvs-position-right">
	<div class="body-wrapper">
		<div class="body-innerwrapper">
			<div id="sp-top-bar">
				<div class="container">
					<div class="container-inner">
						<div class="row">
							<div id="sp-top1" class="col-lg-6">
								<div class="sp-column text-center text-lg-left"></div>
							</div>
							<div id="sp-top2" class="col-lg-6">
								<div class="sp-column text-center text-lg-right">
									<ul class="sp-contact-info">
										<li class="sp-contact-phone"><span class="fa fa-phone" aria-hidden="true"></span> <a href="tel:+62561742110">+62561742110</a>
										</li>
										<li class="sp-contact-mobile"><span class="fa fa-mobile" aria-hidden="true"></span> <a href="tel:+6281522925800">+6281522925800</a>
										</li>
										<li class="sp-contact-email"><span class="fa fa-envelope" aria-hidden="true"></span> <a href="mailto:ult@lpmp-kalbar.net">ult@lpmp-kalbar.net</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include "main-header.php"; ?>
			<section id="sp-section-1">
				<div class="row">
					<div id="sp-title" class="col-lg-12 ">
						<div class="sp-column "></div>
					</div>
				</div>
			</section>
			<?php echo $contents; ?>
			<?php include "footer.php"; ?>
		</div>
	</div>
	<!-- Off Canvas Menu -->
	<div class="offcanvas-overlay"></div>
	<!-- Go to top --> <a href="#" class="sp-scroll-up" aria-label="Scroll Up"><span class="fa fa-chevron-up" aria-hidden="true"></span></a>
</body>
<!-- Mirrored from ult.lpmpkalbar.id/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 May 2020 07:34:16 GMT -->

</html>