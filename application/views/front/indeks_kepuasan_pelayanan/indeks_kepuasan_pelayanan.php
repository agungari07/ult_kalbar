<section id="sp-section-1">
	<div class="row">
		<div id="sp-title" class="col-lg-12 ">
			<div class="sp-column "></div>
		</div>
	</div>
</section>
<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-11">
					<div class="page-content">
						<section id="section-id-1562520295338" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-8">
										<div id="column-id-1562520295337" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1580163602293" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2020/01/27/ikm-semester-2-201911.png" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
												<div id="sppb-addon-1562520295341" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/07/17/ikm-semester-1-2019.jpg" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-4">
										<div id="column-id-1563403333193" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1563403333198" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<div class="sppb-addon-content">Layanan tanpa penilaian ibarat keluarga tanpa nasehat kebijakan. Aktivitas berjalan sendiri tanpa ada yang mengevaluasi.
															<br />
															<br />Ayah menasehati dan menyemangati, ibu tempatnya curahan hati, anak-anak menghormati. Ketika semua anggota keluarga sadar tugasnya seperti ini siklus evaluasi otomatis berjalan sendiri.
															<br />
															<br />Keluarga <a title="ULT LPMP" href="../../index.html" target="_blank" rel="noopener noreferrer">@ultlpmpkalbar</a> demikian pula. Sadar bahwa penilaian bukan hanya untuk diri sendiri saja. Penilaian sesungguhnya berasal dari luar lingkungannya.
															<br />
															<br />Ada plus-minus yang masih perlu kita benahi. Ada kelebihan dan kekurangan yang masih perlu kami sempurnakan. Ada saran, kritik dan masukan yang masih perlu kami wujudkan.
															<br />
															<br />Semua ini kami lakukan untuk sempurnanya layanan. Sempurnanya layanan bukan berarti tercukupinya anggaran, dinginnya ruangan atau bersihnya lingkungan. Sempurnanya layanan adalah masalah terselesaikan penuh dengan rasa kepuasan.</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>