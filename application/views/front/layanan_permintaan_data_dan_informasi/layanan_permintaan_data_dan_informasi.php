<section id="sp-section-1">
	<div class="row">
		<div id="sp-title" class="col-lg-12 ">
			<div class="sp-column "></div>
		</div>
	</div>
</section>
<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-3">
					<div class="page-content">
						<section id="section-id-1550892949816" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-8">
										<div id="column-id-1550892949814" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1559394114810" class="clearfix">
													<div class="sppb-addon sppb-addon-tab ">
														<div class="sppb-addon-content sppb-tab tabs-tab">
															<ul class="sppb-nav sppb-nav-tabs">
																<li class="active"><a data-toggle="sppb-tab" class="left " href="#sppb-tab-1559394114810"><span class="sppb-tab-icon"><i class="fa "></i></span> Prosedur Layanan </a>
																</li>
																<li class=""><a data-toggle="sppb-tab" class="left " href="#sppb-tab-1559394114811"> Formulir Layanan </a>
																</li>
															</ul>
															<div class="sppb-tab-content sppb-tab-tabs-content">
																<div id="sppb-tab-1559394114810" class="sppb-tab-pane sppb-fade active in">
																	<div id="sppb-addon-1559401610805" class="clearfix">
																		<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
																			<div class="sppb-addon-content">
																				<div class="sppb-addon-single-image-container">
																					<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/06/01/permohonan-data.jpg" alt="Image" title="">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div id="sppb-addon-1560084646048" class=" sppb-wow fadeInUp clearfix">
																		<div class="sppb-addon sppb-addon-module ">
																			<div class="sppb-addon-content">
																				<div class="custom">
																					<!-- mibew button -->
																					<p>
																						<a id="mibew-agent-button" href="https://helpdesk.lpmpkalbar.id/chat?locale=id&amp;group=17" target="_blank" rel="noopener noreferrer">
																							<img style="display: block; margin-left: auto; margin-right: auto;" src="https://helpdesk.lpmpkalbar.id/b?i=dua&amp;lang=id&amp;group=17" alt="" border="0" />
																						</a>
																					</p>
																					<!-- / mibew button -->
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div id="sppb-tab-1559394114811" class="sppb-tab-pane sppb-fade">
																	<div id="sppb-addon-1559394114856" class="clearfix">
																		<div class="sppb-addon sppb-addon-module ">
																			<div class="sppb-addon-content">
																				<div id="cf_1" class="convertforms cf cf-img-left cf-form-bottom cf-success-hideform  cf-hasLabels  " style="max-width:800px;background-color:rgba(238, 238, 238, 1);border-style:none;border-width:10px;border-color:#f28395;border-radius:0px;padding:0px;font-family:Arial">
																					<form name="cf1" id="cf1" method="post" action="#">
																						<div class="cf-content-wrap cf-col-16 ">
																							<div class="cf-content cf-col-16">
																								<div class="cf-content-img cf-col-16 cf-text-center cf-col-medium-3">
																									<img alt="" class="" style="width:auto;left:0px ;top:0px" src="<?php echo base_url(); ?>/asset/front/images/logo-lpmp-baru-kecil.png" />
																								</div>
																								<div class="cf-content-text cf-col cf-col-medium-13">
																									<h2>Permintaan Data &amp; Informasi</h2>
																									<p style="line-height: 22px; text-align: left;"><span style="color: #808080;">Silahkan mengisi formulir dibawah ini untuk permintaan data dan informasi<br /></span>
																									</p>
																								</div>
																							</div>
																						</div>
																						<div class="cf-form-wrap cf-col-16 " style="background-color:rgba(195, 195, 195, 1)">
																							<div class="cf-response"></div>
																							<div class="cf-fields">
																								<div class="cf-control-group " data-key="2">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form1_Nama">Nama	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="text" name="cf[Nama]" id="form1_Nama" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="6">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form1_Instansi">Instansi	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="text" name="cf[Instansi]" id="form1_Instansi" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="7">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form1_email">Email	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="email" name="cf[email]" id="form1_email" required class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="8">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form1_telpon">Nomor Telpon	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="tel" name="cf[telpon]" id="form1_telpon" required class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="9">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form1_Tujuan">Tujuan Penggunaan Data	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<div class="cf-list ">
																											<div class="cf-checkbox-group  cf-checkbox-group-required ">
																												<input type="checkbox" name="cf[Tujuan][]" id="form1_Tujuan_0" value="Penelitian" required class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																												<label class="cf-label" for="form1_Tujuan_0" style="font-size: 13px; color: #333333;">Penelitian</label>
																											</div>
																											<div class="cf-checkbox-group  cf-checkbox-group-required ">
																												<input type="checkbox" name="cf[Tujuan][]" id="form1_Tujuan_1" value="Pelengkap Laporan" required class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																												<label class="cf-label" for="form1_Tujuan_1" style="font-size: 13px; color: #333333;">Pelengkap Laporan</label>
																											</div>
																											<div class="cf-checkbox-group  cf-checkbox-group-required ">
																												<input type="checkbox" name="cf[Tujuan][]" id="form1_Tujuan_2" value="Bahan Paparan" required class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																												<label class="cf-label" for="form1_Tujuan_2" style="font-size: 13px; color: #333333;">Bahan Paparan</label>
																											</div>
																											<div class="cf-checkbox-group  cf-checkbox-group-required ">
																												<input type="checkbox" name="cf[Tujuan][]" id="form1_Tujuan_3" value="Lainnya (Tuliskan Pada Penjelasan Dibawah)" required class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																												<label class="cf-label" for="form1_Tujuan_3" style="font-size: 13px; color: #333333;">Lainnya (Tuliskan Pada Penjelasan Dibawah)</label>
																											</div>
																										</div>
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="10">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form1_Penjelasan">Penjelasan	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<textarea name="cf[Penjelasan]" id="form1_Penjelasan" required placeholder="" class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px" rows="3"></textarea>
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="12">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form1_Upload">File Upload	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<div class="cfup-tmpl" style="display:none;">
																											<div class="cfup-file">
																												<div class="cfup-status"></div>
																												<div class="cfup-thumb">
																													<!-- <img data-dz-thumbnail /> -->
																												</div>
																												<div class="cfup-details">
																													<div class="cfup-name" data-dz-name></div>
																													<div class="cfup-error">
																														<div data-dz-errormessage></div>
																													</div>
																													<div class="cfup-progress"><span class="dz-upload" data-dz-uploadprogress></span>
																													</div>
																												</div>
																												<div class="cfup-right">	<span class="cfup-size" data-dz-size></span>
																													<a href="#" class="cfup-remove" data-dz-remove>×</a>
																												</div>
																											</div>
																										</div>
																										<div id="form1_Upload" data-name="cf[Upload]" data-key="12" data-maxfilesize="0" data-maxfiles="1" data-acceptedfiles=".pdf" class="cfupload">
																											<div class="dz-message">	<span>Drag and drop files here or</span>
																												<span class="cfupload-browse">Browse</span>
																											</div>
																										</div>
																										<div class="cf-control-input-desc" style="color:#888888">Silahkan Upload Permohonan Resmi dengan Format PDF</div>
																									</div>
																								</div>
																								<div class="cf-control-group cf-one-third" data-key="1">
																									<div class="cf-control-input">
																										<div class="cf-text-center">
																											<button type="submit" class="cf-btn cf-btn-style-gradient cf-btn-shadow-1 cf-one-third" style="border-radius:5px;padding:11px 15px;color:#ffffff;font-size:14px;background-color:#e86161"> <span class="cf-btn-text">Kirim</span>
																												<span class="cf-spinner-container">
            <span class="cf-spinner">
                <span class="bounce1"></span>
																												<span class="bounce2"></span>
																												<span class="bounce3"></span>
																												</span>
																												</span>
																											</button>
																										</div>
																										<style>
																											#cf_1 .cf-btn:after { 
																											            border-radius: 5px
																											        }
																										</style>
																									</div>
																								</div>
																							</div>
																						</div>
																						<input type="hidden" name="cf[form_id]" value="1">
																						<input type="hidden" name="dee74f3eed08a0f135b09407be6eefb4" value="1" />
																						<div class="cf-field-hp">
																							<label for="cf-field-5ec4dd75c0c41" class="cf-label">Email</label>
																							<input type="text" name="cf[hnpt]" id="cf-field-5ec4dd75c0c41" autocomplete="off" class="cf-input" />
																						</div>
																					</form>
																				</div>
																				<style></style>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-4">
										<div id="column-id-1550892949815" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1550892949819" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Layanan Permintaan 
Data dan Informasi</h3>
														<div class="sppb-addon-content"></div>
													</div>
												</div>
												<div id="sppb-addon-1559401094234" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<div class="sppb-addon-content">Dalam melaksanakan tugas dan fungsi penjaminan mutu pendidikan di daerah,&nbsp; <strong><a href="http://www.lpmpkalbar.id/" target="_blank" rel="noopener noreferrer">LPMP Kalimantan Barat</a></strong> senantiasa mengacu dan berpedoman kepada kebijakan Menteri Pendidikan dan Kebudayaan tentang program dan kegiatan yang terkait dengan penjaminan mutu pendidikan dasar dan menengah di daerah.
															<br />
															<br />LPMP berupaya hadir melayani masyarakat dalam penyajian dan penyebarluasan data dan informasi mutu pendidikan dasar dan pendidikan menengah kepada daerah provinsi dan daerah kabupaten/kota serta pemangku kepentingan lainnya.
															<br />
															<br />Adapun layanan ini meliputi Proses verval NUPTK, Proses verval peserta Pendidikan Profesi Guru dalam jabatan, Dapodikdasmen, Pemetaan mutu pendidikan dan Layanan data Terkait Mutu Pendidikan.</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>