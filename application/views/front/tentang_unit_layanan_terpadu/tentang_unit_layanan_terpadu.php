<section id="sp-section-1">
	<div class="row">
		<div id="sp-title" class="col-lg-12 ">
			<div class="sp-column "></div>
		</div>
	</div>
</section>
<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-8">
					<div class="page-content">
						<section id="section-id-1560123757183" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-4">
										<div id="column-id-1560123757180" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1560123757186" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Unit Layanan Terpadu</h3>
														<div class="sppb-addon-content">Dalam rangka meningkatkan pelayanan kepada masyarakat, Lembaga Penjaminan Mutu Pendidikan Kalimantan Barat menghadirkan layanan publik dan pengaduan secara terpadu yang diperuntukkan bagi masyarakat. Melalui layanan ini, masyarakat dapat meminta informasi, menyampaikan pengaduan, bertanya, berdialog, memberikan saran dan masukan dengan nyaman dan memperoleh kepastian mendapatkan tanggapan yang baik dan profesional.</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-8">
										<div id="column-id-1560123757181" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561890008402" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2020/02/24/1-kecil.png" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section id="section-id-1560123757191" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-4">
										<div id="column-id-1560123757187" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1560123757203" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Layanan Publik</h3>
														<div class="sppb-addon-content">
															<p style="text-align: justify;">Adalah Layanan yang diselenggarakan sesuai tugas dan fungsi LPMP Kalimantan Barat yang sifatnya melayani masyarakat. Layanan ini dapat disampaikan secara langsung datang ke ULT LPMP Kalimantan Barat maupun tidak langsung melalui telepon, SMS/Whatsapp, email, fax, surat dan chat interaktif d laman ini.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-4">
										<div id="column-id-1560123757188" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561890008396" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2020/02/24/2.png" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-4">
										<div id="column-id-1560123757189" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561890008405" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Jenis Layanan</h3>
														<div class="sppb-addon-content">Layana yang diberikan di ULT diantaranya :
															<ol style="text-align: justify;">
																<li>Layanan Supervisi Mutu Pendidikan</li>
																<li>Layanan Data dan Informasi Mutu Pendidikan</li>
																<li>Layanan Kerja Sama Peningkatan Mutu Pendidikan</li>
																<li>Layanan Permohonan Narasumber</li>
																<li>Layanan Peminjaman Fasilitas Wisma/Aula/Kelas</li>
																<li>Layanan Konsultasi Online</li>
															</ol>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section id="section-id-1560123757200" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-8">
										<div id="column-id-1560123757198" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561890008399" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/06/30/foto_ult1.jpg" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-4">
										<div id="column-id-1560123757199" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1560123757194" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Waktu Pelayanan</h3>
														<div class="sppb-addon-content">
															<table style="height: 167px; width: 369px;">
																<tbody>
																	<tr>
																		<td style="width: 121px; text-align: center;"><span style="font-size: 11pt;"><strong>Senin &ndash; Kamis </strong></span>
																		</td>
																		<td style="width: 21px; text-align: center;"><span style="font-size: 11pt;"> = </span>
																		</td>
																		<td style="width: 133.25px;"><span style="font-size: 11pt;"> 08.00 - 16.00 WIB</span>
																		</td>
																	</tr>
																	<tr>
																		<td style="width: 121px; text-align: center;"><span style="font-size: 11pt;"><strong>Jumat</strong></span>
																		</td>
																		<td style="width: 21px; text-align: center;"><span style="font-size: 11pt;">=</span>
																		</td>
																		<td style="width: 133.25px;"><span style="font-size: 11pt;">08.00 - 16.30 WIB</span>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section id="section-id-1561890008424" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-7">
										<div id="column-id-1561890008423" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1560123757197" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Tempat Pelayanan</h3>
														<div class="sppb-addon-content">
															<p style="text-align: justify;">Unit Layanan Terpadu LPMP Kalimantan Barat, Jalan Abdul Muis, Tanjung Hulu, Pontianak Timur.</p>
														</div>
													</div>
												</div>
												<div id="sppb-addon-1561890008417" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<div class="sppb-addon-content">Kami berharap melalui layanan ini, masyarakat dapat memanfaatkannya dengan baik dan kami berupaya untuk terus meningkatkan pelayanan yang ada di Lembaga Penjaminan Mutu Pendidikan Kalimantan Barat.</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-5">
										<div id="column-id-1561890008437" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561890008440" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2020/02/24/3.jpg" alt="Image" title="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section id="section-id-1583458097791" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-12">
										<div id="column-id-1583458097790" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1583458202415" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<h3 class="sppb-addon-title">Tarif Sewa Layanan Lembaga Mutu Pendidikan Kalimantan Barat</h3>
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2020/03/06/tarif-sewa-layanan-lpmp---art-paper-a3---2-lembar.jpg" alt="Image" title="Tarif Sewa Layanan Lembaga Mutu Pendidikan Kalimantan Barat">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<section id="section-id-1580165216066" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-6">
										<div id="column-id-1580165216064" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1580165216069" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<h3 class="sppb-addon-title">Maklumat Pelayanan</h3>
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2020/03/06/maklumat-pelayanan-okta.jpg" alt="Image" title="Maklumat Pelayanan">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-6">
										<div id="column-id-1580165216065" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1580165216072" class="clearfix">
													<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
														<h3 class="sppb-addon-title">Maklumat Keterbukaan Informasi Publik</h3>
														<div class="sppb-addon-content">
															<div class="sppb-addon-single-image-container">
																<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2020/03/06/maklumat-keterbukaan-informasi-publik---art-paper-a3---1-lembar.jpg" alt="Image" title="Maklumat Keterbukaan Informasi Publik">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>