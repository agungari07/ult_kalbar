<header id="sp-header">
	<div class="container">
		<div class="container-inner">
			<div class="row">
				<div id="sp-logo" class="col-8 col-lg-3">
					<div class="sp-column">
						<div class="logo">
							<a href="<?php base_url(); ?>">
								<img class="logo-image d-none d-lg-inline-block" src="<?php echo base_url(); ?>asset/front/images/ult.png" alt="ULT LPMP Kalimantan Barat">
								<img class="logo-image-phone d-inline-block d-lg-none" src="<?php echo base_url(); ?>asset/front/images/ult.png" alt="ULT LPMP Kalimantan Barat">
							</a>
						</div>
					</div>
				</div>
				<?php include "menu-front.php"; ?>
			</div>
		</div>
	</div>
</header>