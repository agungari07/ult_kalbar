<section id="sp-section-1">
	<div class="row">
		<div id="sp-title" class="col-lg-12 ">
			<div class="sp-column "></div>
		</div>
	</div>
</section>
<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-15">
					<div class="page-content">
						<section id="section-id-1580172886080" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-12">
										<div id="column-id-1580172886079" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1580172960846" class="clearfix">
													<div class="sppb-addon sppb-addon-module ">
														<div class="sppb-addon-content">
															<div class="custom">
																<style type="text/css">
																	.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
																	.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-top-width:1px;border-bottom-width:1px;border-color:#ccc;color:#333;background-color:#fff;}
																	.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-top-width:1px;border-bottom-width:1px;border-color:#ccc;color:#333;background-color:#f0f0f0;}
																	.tg .tg-buh4{background-color:#f9f9f9;text-align:left;vertical-align:top}
																	.tg .tg-shda{font-weight:bold;border-color:#002b36;text-align:left;vertical-align:top}
																	.tg .tg-ocny{background-color:#f9f9f9;border-color:#002b36;text-align:left;vertical-align:top}
																	.tg .tg-d8bh{border-color:#002b36;text-align:left;vertical-align:top}
																</style>
																<table class="tg" style="undefined;table-layout: fixed; width:  100%">
																	<colgroup>
																		<col style="width: 100%">
																	</colgroup>
																	<tr>
																		<th class="tg-shda">Laporan Unit Layanan Terpadu Lembaga Penjaminan Mutu Pendidikan Kalimantan Barat Tahun 2019</th>
																	</tr>
																	<tr>
																		<td class="tg-ocny">1. Laporan Unit Layanan Terpadu Semester I 2019 <a src="<?php echo base_url(); ?>/asset/front/laporanult/Laporan%20ULT%20Semester%201%20Ok.pdf">&lt;Unduh&gt;</a>
																			<br>
																		</td>
																	</tr>
																	<tr>
																		<td class="tg-d8bh">2. Laporan Unit Layanan Terpadu Semester II 2019 <a href="https://laporan ult semester 2 ok.pdf/">&lt;Unduh&gt;</a>
																			<br>
																		</td>
																	</tr>
																	<tr>
																		<td class="tg-buh4">3. Laporan Unit Layanan Terpadu Tahun 2019 <a href="https://laporan ult tahun 2019.pdf/">&lt;Unduh&gt;</a>
																		</td>
																	</tr>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>