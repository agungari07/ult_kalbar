<footer id="sp-footer">
	<div class="container">
		<div class="container-inner">
			<div class="row">
				<div id="sp-footer1" class="col-lg-12 ">
					<div class="sp-column "><span class="sp-copyright">© 2019 Lembaga Penjaminan Mutu Pendidikan Kalimantan Barat</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>