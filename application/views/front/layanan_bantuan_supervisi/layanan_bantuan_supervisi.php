<section id="sp-section-1">
	<div class="row">
		<div id="sp-title" class="col-lg-12 ">
			<div class="sp-column "></div>
		</div>
	</div>
</section>
<section id="sp-main-body">
	<div class="row">
		<main id="sp-component" class="col-lg-12 " role="main">
			<div class="sp-column ">
				<div id="system-message-container"></div>
				<div id="sp-page-builder" class="sp-page-builder  page-4">
					<div class="page-content">
						<section id="section-id-1552046243918" class="sppb-section ">
							<div class="sppb-row-container">
								<div class="sppb-row">
									<div class="sppb-col-md-4">
										<div id="column-id-1552046243916" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561887628167" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<h3 class="sppb-addon-title">Layanan 
Bantuan Supervisi</h3>
														<div class="sppb-addon-content"></div>
													</div>
												</div>
												<div id="sppb-addon-1552046243921" class="clearfix">
													<div class="sppb-addon sppb-addon-text-block 0  ">
														<div class="sppb-addon-content">
															<br />Mutu pendidikan merupakan salah satu tolok ukur yang menentukan martabat atau kemajuan suatu bangsa.&nbsp; Oleh karena itu, bangsa yang maju akan selalu menaruh perhatian besar terhadap dunia pendidikannya, dengan melakukan berbagai upaya untuk meningkatkan mutu pendidikan, salah satunya adalah dengan supervisi.
															<br />
															<br />Dengan kata lain, supervisi pendidikan adalah pembinaan ke arah perbaikan situasi pendidikan dan pengajaran pada umumnya serta peningkatan mutu mengajar dan belajar pada khususnya. Untuk mencapai mutu pendidikan yang lebih baik dan berkualitas&nbsp; <strong><a href="http://lpmpkalbar.id/" target="_blank" rel="noopener noreferrer">LPMP Kalimantan Barat</a></strong> menyediakan suatu bentuk pelayanan Supervisi Mutu Pendidikan yang terprogram, terencana dan berlangsung kontinyu agar kegiatan belajar mengajar dapat berjalan lebih baik dan berkualitas yang akhirnya dapat meningkatkan mutu pendidikan seperti yang kita harapkan.
															<br />
															<br />Untuk memenuhi layanan ini, Dinas Pendidikan dan Pengurus yayasan terkait dapat mengajukan surat permohonan pengajuan Supervisi Mutu Pendidikan ke <strong><a href="http://lpmpkalbar.id/" target="_blank" rel="noopener noreferrer">LPMP Kalimantan Barat</a></strong> untuk ditindaklanjuti sesuai kebutuhan.</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="sppb-col-md-8">
										<div id="column-id-1552046243917" class="sppb-column">
											<div class="sppb-column-addons">
												<div id="sppb-addon-1561887273870" class="clearfix">
													<div class="sppb-addon sppb-addon-tab ">
														<div class="sppb-addon-content sppb-tab tabs-tab">
															<ul class="sppb-nav sppb-nav-tabs">
																<li class="active"><a data-toggle="sppb-tab" class="left " href="#sppb-tab-1561887273870"><span class="sppb-tab-icon"><i class="fa "></i></span> Prosedur Layanan </a>
																</li>
																<li class=""><a data-toggle="sppb-tab" class="left " href="#sppb-tab-1561887273871"> Formulir Layanan </a>
																</li>
															</ul>
															<div class="sppb-tab-content sppb-tab-tabs-content">
																<div id="sppb-tab-1561887273870" class="sppb-tab-pane sppb-fade active in">
																	<div id="sppb-addon-1561887273944" class="clearfix">
																		<div class="sppb-addon sppb-addon-single-image sppb-text-center ">
																			<div class="sppb-addon-content">
																				<div class="sppb-addon-single-image-container">
																					<img class="sppb-img-responsive" src="<?php echo base_url(); ?>/asset/front/images/2019/06/28/supervisi.png" alt="Image" title="">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div id="sppb-tab-1561887273871" class="sppb-tab-pane sppb-fade">
																	<div id="sppb-addon-1561887628229" class="clearfix">
																		<div class="sppb-addon sppb-addon-module ">
																			<div class="sppb-addon-content">
																				<div id="cf_5" class="convertforms cf cf-img-left cf-form-bottom cf-success-hideform  cf-hasLabels  " style="max-width:800px;background-color:rgba(238, 238, 238, 1);border-style:none;border-width:10px;border-color:#f28395;border-radius:0px;padding:0px;font-family:Arial">
																					<form name="cf5" id="cf5" method="post" action="#">
																						<div class="cf-content-wrap cf-col-16 ">
																							<div class="cf-content cf-col-16">
																								<div class="cf-content-img cf-col-16 cf-text-center cf-col-medium-3">
																									<img alt="" class="" style="width:auto;left:0px ;top:0px" src="<?php echo base_url(); ?>/asset/front/images/logo-lpmp-baru-kecil.png" />
																								</div>
																								<div class="cf-content-text cf-col cf-col-medium-13">
																									<h2>Permintaan Bantuan Layanan Supervisi</h2>
																									<p style="line-height: 22px; text-align: left;"><span style="color: #808080;">Silahkan mengisi formulir dibawah ini untuk permintaan layanan supervisi<br /></span>
																									</p>
																								</div>
																							</div>
																						</div>
																						<div class="cf-form-wrap cf-col-16 " style="background-color:rgba(195, 195, 195, 1)">
																							<div class="cf-response"></div>
																							<div class="cf-fields">
																								<div class="cf-control-group " data-key="2">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form5_Nama">Nama	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="text" name="cf[Nama]" id="form5_Nama" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="6">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form5_Instansi">Instansi	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="text" name="cf[Instansi]" id="form5_Instansi" required class="cf-input cf-input-shadow-0 cf-one-half" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="7">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form5_email">Email	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="email" name="cf[email]" id="form5_email" required class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="8">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form5_telpon">Nomor Telpon	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<input type="tel" name="cf[telpon]" id="form5_telpon" required class="cf-input cf-input-shadow-0 cf-one-third" style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px">
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="10">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form5_Penjelasan">Penjelasan Bantuan Layanan Supervisi Yang diinginkan	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<textarea name="cf[Penjelasan]" id="form5_Penjelasan" required placeholder="" class="cf-input cf-input-shadow-0 " style="text-align:left;color:#333333;background-color:#eeeeee;border-color:#ffffff;border-radius:5px;font-size:13px;padding:11px 20px" rows="3"></textarea>
																									</div>
																								</div>
																								<div class="cf-control-group " data-key="12">
																									<div class="cf-control-label">
																										<label class="cf-label" style="color:#888888;font-size:13px" for="form5_Upload">File Upload	<span class="cf-required-label">*</span>
																										</label>
																									</div>
																									<div class="cf-control-input">
																										<div class="cfup-tmpl" style="display:none;">
																											<div class="cfup-file">
																												<div class="cfup-status"></div>
																												<div class="cfup-thumb">
																													<!-- <img data-dz-thumbnail /> -->
																												</div>
																												<div class="cfup-details">
																													<div class="cfup-name" data-dz-name></div>
																													<div class="cfup-error">
																														<div data-dz-errormessage></div>
																													</div>
																													<div class="cfup-progress"><span class="dz-upload" data-dz-uploadprogress></span>
																													</div>
																												</div>
																												<div class="cfup-right">	<span class="cfup-size" data-dz-size></span>
																													<a href="#" class="cfup-remove" data-dz-remove>×</a>
																												</div>
																											</div>
																										</div>
																										<div id="form5_Upload" data-name="cf[Upload]" data-key="12" data-maxfilesize="0" data-maxfiles="1" data-acceptedfiles=".pdf" class="cfupload">
																											<div class="dz-message">	<span>Drag and drop files here or</span>
																												<span class="cfupload-browse">Browse</span>
																											</div>
																										</div>
																										<div class="cf-control-input-desc" style="color:#888888">Silahkan Upload Permohonan Resmi dengan Format PDF</div>
																									</div>
																								</div>
																								<div class="cf-control-group cf-one-third" data-key="1">
																									<div class="cf-control-input">
																										<div class="cf-text-center">
																											<button type="submit" class="cf-btn cf-btn-style-gradient cf-btn-shadow-1 cf-one-third" style="border-radius:5px;padding:11px 15px;color:#ffffff;font-size:14px;background-color:#e86161"> <span class="cf-btn-text">Kirim</span>
																												<span class="cf-spinner-container">
            <span class="cf-spinner">
                <span class="bounce1"></span>
																												<span class="bounce2"></span>
																												<span class="bounce3"></span>
																												</span>
																												</span>
																											</button>
																										</div>
																										<style>
																											#cf_5 .cf-btn:after { 
																											            border-radius: 5px
																											        }
																										</style>
																									</div>
																								</div>
																							</div>
																						</div>
																						<input type="hidden" name="cf[form_id]" value="5">
																						<input type="hidden" name="dee74f3eed08a0f135b09407be6eefb4" value="1" />
																						<div class="cf-field-hp">
																							<label for="cf-field-5ec4dd7b684b2" class="cf-label">Website</label>
																							<input type="text" name="cf[hnpt]" id="cf-field-5ec4dd7b684b2" autocomplete="off" class="cf-input" />
																						</div>
																					</form>
																				</div>
																				<style></style>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</main>
	</div>
</section>