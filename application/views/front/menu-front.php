<div id="sp-menu" class="col-4 col-lg-9">
	<div class="sp-column">
		<nav class="sp-megamenu-wrapper" role="navigation"><a id="offcanvas-toggler" aria-label="Navigation" class="offcanvas-toggler-right d-block d-lg-none" href="#"><i class="fa fa-bars" aria-hidden="true" title="Navigation"></i></a>
			<ul class="sp-megamenu-parent menu-animation-fade-up d-none d-lg-block">
				<li class="sp-menu-item current-item active"><a href="<?php echo base_url(); ?>">Beranda</a>
				</li>
				<li class="sp-menu-item sp-has-child"><a href="#">Unit Layanan Terpadu</a>
					<div class="sp-dropdown sp-dropdown-main sp-menu-right" style="width: 240px;">
						<div class="sp-dropdown-inner">
							<ul class="sp-dropdown-items">
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>selayang-pandang/tentang-unit-layanan-terpadu">Tentang ULT</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>selayang-pandang/keseharian-unit-layanan-terpadu">Keseharian ULT</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>selayang-pandang/pengunjung-unit-layanan-terpadu">Pengunjung ULT</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>selayang-pandang/indeks-kepuasan-pelayanan">Indeks Kepuasan Pelayanan</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>selayang-pandang/laporan-ult">Laporan ULT</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li class="sp-menu-item sp-has-child"><a href="#">Layanan Publik</a>
					<div class="sp-dropdown sp-dropdown-main sp-menu-right" style="width: 240px;">
						<div class="sp-dropdown-inner">
							<ul class="sp-dropdown-items">
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>layanan-publik/layanan-peminjaman-fasilitas">Peminjaman Fasilitas</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>layanan-publik/layanan-permintaan-narasumber">Permintaan Narasumber</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>layanan-publik/layanan-permintaan-data-dan-informasi">Permintaan Data &amp; Informasi</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>layanan-publik/layanan-kerjasama">Pengajuan Kerjasama</a>
								</li>
								<li class="sp-menu-item"><a href="<?php echo base_url(); ?>layanan-publik/layanan-bantuan-supervisi">Bantuan Supervisi</a>
								</li>
							</ul>
						</div>
					</div>
				</li>
			</ul>
		</nav>
	</div>
</div>