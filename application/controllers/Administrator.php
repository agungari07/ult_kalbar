<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Administrator extends CI_Controller
{
	function index()
	{
		if (isset($_POST['submit'])) {
			$username = $this->input->post('a');
			$password = md5($this->input->post('b'));
			$cek = $this->model_users->cek_login($username, $password);
			$row = $cek->row_array();
			$total = $cek->num_rows();
			if ($total > 0) {
				$this->session->set_userdata('upload_image_file_manager', true);
				$this->session->set_userdata(array(
					'username' => $row['username'],
					'level' => $row['level']
				));
				redirect('administrator/home');
			} else {
				$data['title'] = 'Administrator &rsaquo; Log In';
				$this->load->view('administrator/view_login', $data);
			}
		} else {
			if ($this->session->level != '') {
				redirect('administrator/home');
			} else {
				$data['title'] = 'Administrator &rsaquo; Log In';
				$this->load->view('administrator/view_login', $data);
			}
		}
	}

	function home()
	{
		cek_session_admin();
		$this->template->load('administrator/template', 'administrator/view_home');
	}

	function identitaswebsite()
	{
		cek_session_admin();
		if (isset($_POST['submit'])) {
			$this->model_identitas->identitas_update();
			redirect('administrator/identitaswebsite');
		} else {
			$data['record'] = $this->model_identitas->identitas()->row_array();
			$this->template->load('administrator/template', 'administrator/mod_identitas/view_identitas', $data);
		}
	}


	// Controller Modul List Peminjam Fasilitas

	function peminjam()
	{
		cek_session_admin();
		$data['record'] = $this->model_peminjam->list_peminjam();
		$this->template->load('administrator/template', 'administrator/mod_peminjam/view_peminjam', $data);
	}




	function delete_peminjam()
	{
		$id = $this->uri->segment(3);
		$data = $this->model_peminjam->peminjam_edit($id)->row_array();
		$filename = $data['file'];

		$Path = FCPATH . 'asset/uploads\\' . $filename;
		unlink($Path);

		$this->model_peminjam->peminjam_delete($id);
		redirect('administrator/peminjam');
	}

	function edit_peminjam()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);

		$data['rows'] = $this->model_peminjam->peminjam_edit($id)->row_array();
		$this->template->load('administrator/template', 'administrator/mod_peminjam/view_detail', $data);
	}

	function donwload_file_peminjam()
	{

		$id = $this->uri->segment(3);
		$file = $this->model_peminjam->getftile($id)->file;
		$this->load->helper('download');
		$file = './asset/uploads/' . $file;
		force_download($file, NULL);
	}




	// Controller Modul User

	function manajemenuser()
	{
		cek_session_admin();
		$data['record'] = $this->model_users->users();
		$this->template->load('administrator/template', 'administrator/mod_users/view_users', $data);
	}

	function tambah_manajemenuser()
	{
		cek_session_admin();
		$id = $this->session->username;
		if (isset($_POST['submit'])) {
			$this->model_users->users_tambah();
			redirect('administrator/manajemenuser');
		} else {
			$data['rows'] = $this->model_users->users_edit($id)->row_array();
			$this->template->load('administrator/template', 'administrator/mod_users/view_users_tambah', $data);
		}
	}

	function edit_manajemenuser()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])) {
			$this->model_users->users_update();
			redirect('administrator/manajemenuser');
		} else {
			$data['rows'] = $this->model_users->users_edit($id)->row_array();
			$this->template->load('administrator/template', 'administrator/mod_users/view_users_edit', $data);
		}
	}

	function delete_manajemenuser()
	{
		$id = $this->uri->segment(3);
		$this->model_users->users_delete($id);
		redirect('administrator/manajemenuser');
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
