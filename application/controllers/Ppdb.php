<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Ppdb extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model(array(
			'model_kab', 'model_jenjang', 'model_ppdb',
		));
	}

	public function index()
	{
		$data['judul_layanan'] 	= "Pengaduan PPBD Tahun 2020";
		$data['flash']			= false;
		$data['kab']			= $this->model_kab->getall();
		$data['jenjang']		= $this->model_jenjang->getall();
		$this->template->load('home/template', 'home/pengaduan_ppdb/main', $data);
	}

	// simpan
	public function save()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('id_kab', 'id_kab', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');

		$this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
		$this->form_validation->set_error_delimiters('<span class="help-block"></span>');

		if ($this->form_validation->run() == FALSE) {

			$data['judul_layanan'] 	= "Pengaduan PPBD Tahun 2020";
			$data['flash']			= 1;
			$data['kab']			= $this->model_kab->getall();
			$data['jenjang']		= $this->model_jenjang->getall();
			$this->template->load('home/template', 'home/pengaduan_ppdb/main', $data);
		}else{

			$config['upload_path']         = './asset/uploads/ppdb/';  // folder upload 
			$config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|pdf'; // jenis file
			$config['max_size']             = 2000;
			$config['max_width']            = 1024;
			$config['max_height']           = 768;
			$this->load->library('upload', $config);



			$file 				= $this->upload->data();
			$gambar 			= $file['file_name'];

			$data = array(
				'nama' 				=> $this->input->post('nama'),
				'id_kab' 			=> $this->input->post('id_kab'),
				'id_jenjang' 		=> $this->input->post('id_jenjang'),
				'email' 			=> $this->input->post('email'),
				'no_hp' 			=> $this->input->post('no_hp'),
				'deskripsi' 		=> $this->input->post('deskripsi'),
				'file'				=> $gambar,
				'status'			=> '0',
			);


			$status = $this->model_ppdb->save($data);

			$data2['judul_layanan'] = "Pengaduan PPBD Tahun 2020";
			$data2['flash']			= 2;
			$data2['kab']			= $this->model_kab->getall();
			$data2['jenjang']		= $this->model_jenjang->getall();
			$this->template->load('home/template', 'home/pengaduan_ppdb/main', $data2);
		}


	}


	// Controller Modul List Peminjam Fasilitas

	function view()
	{
		cek_session_admin();
		$data['record'] = $this->model_ppdb->list_ppdb();
		$this->template->load('administrator/template', 'administrator/mod_ppdb/view_ppdb', $data);
	}


	function delete_ppdb()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);
		$data = $this->model_ppdb->get_edit($id)->row_array();
		$filename = $data['file'];

		$Path = FCPATH . 'asset/uploads/ppdb/\\' . $filename;
		unlink($Path);

		$this->model_ppdb->delete($id);
		redirect('ppdb/view');
	}

	function detail_ppdb()
	{
		cek_session_admin();
		$id = $this->uri->segment(3);

		$data = array(
			'status'			=> '1',
		);

		$status = $this->model_ppdb->update($id, $data);

		$data['rows'] = $this->model_ppdb->get_edit($id)->row_array();
		$this->template->load('administrator/template', 'administrator/mod_ppdb/view_detail', $data);
	}

	function donwload_file_ppdb()
	{
		cek_session_admin();

		$id = $this->uri->segment(3);
		$file = $this->model_ppdb->getftile($id)->file;
		$this->load->helper('download');
		$file = './asset/uploads/ppdb/' . $file;
		force_download($file, NULL);
	}

	public function update_status($id = null)
	{
		cek_session_admin();
		$data = array(
			'status'			=> '2',
		);

		$status = $this->model_ppdb->update($id, $data);
		redirect('ppdb/view');
	}
}
