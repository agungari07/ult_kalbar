<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Utama extends CI_Controller
{
	public function index()
	{
		$this->template->load('home/template', 'home/main');
	}

	public function pengaduan_ppdb()
	{
		$this->template->load('home/template', 'home/pengaduan_ppdb/main');
	}

	public function tentang_unit_layanan_terpadu()
	{
		$this->template->load('front/template', 'front/tentang_unit_layanan_terpadu/tentang_unit_layanan_terpadu');
	}

	public function keseharian_unit_layanan_terpadu()
	{
		$this->template->load('front/template', 'front/keseharian_unit_layanan_terpadu/keseharian_unit_layanan_terpadu');
	}

	public function pengunjung_unit_layanan_terpadu()
	{
		$this->template->load('front/template', 'front/pengunjung_unit_layanan_terpadu/pengunjung_unit_layanan_terpadu');
	}

	public function indeks_kepuasan_pelayanan()
	{
		$this->template->load('front/template', 'front/indeks_kepuasan_pelayanan/indeks_kepuasan_pelayanan');
	}

	public function laporan_ult()
	{
		$this->template->load('front/template', 'front/laporan_ult/laporan_ult');
	}


	public function layanan_peminjaman_fasilitas()
	{
		$this->template->load('front/template', 'front/layanan_peminjaman_fasilitas/layanan_peminjaman_fasilitas');
	}

	public function layanan_permintaan_narasumber()
	{
		$this->template->load('front/template', 'front/layanan_permintaan_narasumber/layanan_permintaan_narasumber');
	}

	public function layanan_permintaan_data_dan_informasi()
	{
		$this->template->load('front/template', 'front/layanan_permintaan_data_dan_informasi/layanan_permintaan_data_dan_informasi');
	}

	public function layanan_kerjasama()
	{
		$this->template->load('front/template', 'front/layanan_kerjasama/layanan_kerjasama');
	}

	public function layanan_bantuan_supervisi()
	{
		$this->template->load('front/template', 'front/layanan_bantuan_supervisi/layanan_bantuan_supervisi');
	}

	// simpan
	public function save()
	{

		$config['upload_path']         = './asset/uploads/';  // folder upload 
		$config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|pdf'; // jenis file
		$config['max_size']             = 2000;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;
		$this->load->library('upload', $config);


		if (!$this->upload->do_upload('filename2')) {
			echo 'invalid';
		} else {

			$file 				= $this->upload->data();
			$gambar 			= $file['file_name'];

			$data = array(
				'nama' 				=> $this->input->post('Nama'),
				'instansi' 			=> $this->input->post('Instansi'),
				'email'				=> $this->input->post('email'),
				'no_telp' 			=> $this->input->post('telpon'),
				'fasilitas'			=> implode("<br>", $this->input->post('Fasilitas')),
				'tanggal_pelaksaan'	=> $this->input->post('Mulai'),
				'jumlah_peserta'	=> $this->input->post('Peserta'),
				'deskripsi'			=> $this->input->post('Penjelasan'),
				'file'				=> $gambar
			);

			$this->load->model('Model_pengaduan', 'model', true);
			$status = $this->model->save($data);

			if ($status) {
				echo 'ok';
			} else {
				echo 'invalid';
			}
		}
	}
}
