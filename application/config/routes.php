<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $module = base_url();
$route['default_controller']                                         	= 'Utama';
$route["pengaduan/ppdb"]                                             	= "ppdb";
$route["pengaduan/ppdb/save"]                                  			= "ppdb/save";
// $route["selayang-pandang/tentang-unit-layanan-terpadu"]              = "utama/tentang_unit_layanan_terpadu";
// $route["selayang-pandang/keseharian-unit-layanan-terpadu"]           = "utama/keseharian_unit_layanan_terpadu";
// $route["selayang-pandang/pengunjung-unit-layanan-terpadu"]           = "utama/pengunjung_unit_layanan_terpadu";
// $route["selayang-pandang/indeks-kepuasan-pelayanan"]                 = "utama/indeks_kepuasan_pelayanan";
// $route["selayang-pandang/laporan-ult"]                               = "utama/laporan_ult";
$route["layanan-publik/layanan-peminjaman-fasilitas"]                = "utama/layanan_peminjaman_fasilitas";
// $route["layanan-publik/utama/save"]                                  = "utama/save";
// $route["layanan-publik/layanan-permintaan-narasumber"]               = "utama/layanan_permintaan_narasumber";
// $route["layanan-publik/layanan-permintaan-data-dan-informasi"]       = "utama/layanan_permintaan_data_dan_informasi";
// $route["layanan-publik/layanan-kerjasama"]                           = "utama/layanan_kerjasama";
// $route["layanan-publik/layanan-bantuan-supervisi"]                   = "utama/layanan_bantuan_supervisi";
$route['404_override']                                               = '';
$route['translate_uri_dashes']                                       = FALSE;
