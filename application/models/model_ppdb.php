<?php 
class model_ppdb extends CI_model{

    private $tbl = 'tb_ppdb';


    function getall()
    {
        $this->db->order_by('id');
        $query = $this->db->get($this->tbl);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }

    function list_ppdb(){
        return $this->db->query("SELECT tp.id_ppdb, tp.nama, tp.email, tp.no_hp, tp.deskripsi, tp.file, case when status='0' then 'Belum Dilihat' when status ='1' then 'Terbaca' else 'Selesai' end as status, rk.name as kabupaten, rj.name as jenjang 
                                FROM tb_ppdb tp 
                                inner join tb_ref_kab rk on tp.id_kab= rk.id
                                inner join tb_ref_jenjang rj on tp.id_jenjang= rj.id
                                ORDER BY tp.status ASC");
    }

    function get_edit($id){
        return $this->db->query("SELECT tp.id_ppdb, tp.nama, tp.email, tp.no_hp, tp.deskripsi, tp.file, case when status='0' then 'Belum Dilihat' when status ='1' then 'Terbaca' else 'Selesai' end as status, rk.name as kabupaten, rj.name jenjang 
                                FROM tb_ppdb tp 
                                inner join tb_ref_kab rk on tp.id_kab= rk.id
                                inner join tb_ref_jenjang rj on tp.id_jenjang= rj.id where id_ppdb='$id'");
    }

    function delete($id){
        return $this->db->query("DELETE FROM tb_ppdb where id_ppdb='$id'");
    }

    function save($data) {
        $data = $this->db->insert($this->tbl,$data);
        return $data;
    }

    function update($id, $data) {
        $this->db->where('id_ppdb', $id);
        $this->db->update($this->tbl,$data);
    }

    function getftile($id){
        $data = $this->db->query("SELECT file FROM tb_ppdb where id_ppdb='$id'")->row();
        return $data;
    }

}