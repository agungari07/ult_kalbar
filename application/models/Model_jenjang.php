<?php 
class model_jenjang extends CI_model{

    private $tbl = 'tb_ref_jenjang';


    function getall()
    {
        $this->db->order_by('id');
        $query = $this->db->get($this->tbl);
        if($query->num_rows()!==0)
        {
            return $query->result();
        }
        else
            return FALSE;
    }

    function get_edit($id){
        return $this->db->query("SELECT * FROM tb_ref_jenjang where id='$id'");
    }

    function delete($id){
        return $this->db->query("DELETE FROM tb_ref_jenjang where id='$id'");
    }

    function save($data) {
        $data = $this->db->insert($this->tbl,$data);
        return $data;
    }

}